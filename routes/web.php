<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', '\Mgzaspuc\Users\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', '\Mgzaspuc\Users\Http\Controllers\Auth\LoginController@login');
Route::post('logout', '\Mgzaspuc\Users\Http\Controllers\Auth\LoginController@logout')->name('logout');
Route::get('logout', '\Mgzaspuc\Users\Http\Controllers\Auth\LoginController@logout');
  
Route::post('password/email', '\Mgzaspuc\Users\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset', '\Mgzaspuc\Users\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/reset', '\Mgzaspuc\Users\Http\Controllers\Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset/{token}', '\Mgzaspuc\Users\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('register', '\Mgzaspuc\Users\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/marca/{idProvider}/{nameProvider}', 'HomeController@index');
Route::get('/produto/detalhe/{idProduto}', 'ProductController@show')->name('produto_detalhe');

//Página de contato
Route::get('/contato', 'ContactController@display')->name('contato');
Route::post('/contato', 'ContactController@display')->name('contato');

Route::group(['middleware' => 'auth:web'], function() {
    Route::group(['middleware' => 'can:allow-admin'], function() {
        // Routers for Products
        Route::get('/produtos', '\Mgzaspuc\Products\Http\Controllers\ProductController@index')->name('produtos');
        Route::get('/produto/novo', '\Mgzaspuc\Products\Http\Controllers\ProductController@create')->name('produto_novo');
        Route::post('/produto/salvar', '\Mgzaspuc\Products\Http\Controllers\ProductController@store')->name('produto_salvar');
        Route::get('/produto/editar/{id}', '\Mgzaspuc\Products\Http\Controllers\ProductController@edit')->name('produto_editar');
        Route::post('/produto/atualizar', '\Mgzaspuc\Products\Http\Controllers\ProductController@update')->name('produto_atualizar');
        Route::post('/produto/excluir', '\Mgzaspuc\Products\Http\Controllers\ProductController@destroy')->name('produto_excluir'); 

        // Routers for Providers
        Route::get('/fornecedores', '\Mgzaspuc\Providers\Http\Controllers\ProviderController@index')->name('fornecedores');
        Route::get('/fornecedor/novo', '\Mgzaspuc\Providers\Http\Controllers\ProviderController@create')->name('fornecedor_novo');
        Route::post('/fornecedor/salvar', '\Mgzaspuc\Providers\Http\Controllers\ProviderController@store')->name('fornecedor_salvar');
        Route::get('/fornecedor/editar/{id}', '\Mgzaspuc\Providers\Http\Controllers\ProviderController@edit')->name('fornecedor_editar');
        Route::post('/fornecedor/atualizar', '\Mgzaspuc\Providers\Http\Controllers\ProviderController@update')->name('fornecedor_atualizar');
        Route::post('/fornecedor/excluir', '\Mgzaspuc\Providers\Http\Controllers\ProviderController@destroy')->name('fornecedor_excluir');

        // Routers for User
        Route::get('/usuarios', '\Mgzaspuc\Users\Http\Controllers\UserController@index')->name('usuarios');
        Route::get('/usuario/novo', '\Mgzaspuc\Users\Http\Controllers\UserController@create')->name('usuario_novo');
        Route::post('/usuario/salvar', '\Mgzaspuc\Users\Http\Controllers\UserController@store')->name('usuario_salvar');
        Route::get('/usuario/editar/{id}', '\Mgzaspuc\Users\Http\Controllers\UserController@edit')->name('usuario_editar');
        Route::post('/usuario/atualizar', '\Mgzaspuc\Users\Http\Controllers\UserController@update')->name('usuario_atualizar');
        Route::post('/usuario/excluir', '\Mgzaspuc\Users\Http\Controllers\UserController@destroy')->name('usuario_excluir');
    });
    
    Route::group(['middleware' => 'can:allow-admin-seller'], function() {

        Route::get('/admin', 'AdminController@index')->name('admin');

        // Routers for People
        Route::get('/pessoas', '\Mgzaspuc\People\Http\Controllers\PeopleController@index')->name('pessoas');
        Route::get('/pessoa/novo', '\Mgzaspuc\People\Http\Controllers\PeopleController@create')->name('pessoa_novo');
        Route::post('/pessoa/salvar', '\Mgzaspuc\People\Http\Controllers\PeopleController@store')->name('pessoa_salvar');
        Route::get('/pessoa/editar/{id}', '\Mgzaspuc\People\Http\Controllers\PeopleController@edit')->name('pessoa_editar');
        Route::post('/pessoa/atualizar', '\Mgzaspuc\People\Http\Controllers\PeopleController@update')->name('pessoa_atualizar');
        Route::post('/pessoa/excluir', '\Mgzaspuc\People\Http\Controllers\PeopleController@destroy')->name('pessoa_excluir');          
    });
});


Route::get('/obter/produto/fornecedor/1', '\Mgzaspuc\Products\Http\Controllers\IntegrationController@getProductProvider1')->name('obter_produto_fornecedor_1');
Route::get('/obter/produto/fornecedor/2', '\Mgzaspuc\Products\Http\Controllers\IntegrationController@getProductProvider2')->name('obter_produto_fornecedor_2');
