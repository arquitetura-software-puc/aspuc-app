<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Mgzaspuc\People\Seeder\PeopleTableSeeder::class);
        $this->call(Mgzaspuc\Users\Seeder\UsersTableSeeder::class);
    }
}
