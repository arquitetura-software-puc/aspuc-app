<?php

class Product
{
    public function getListProduct()
    {
        return [
            [
                'name' => 'Produto exemplo F',
                'description' => 'Descrição aqui',
                'price' => 10.22,
                'price_discount' => 10.00,
                'amount' => 15
            ],
            [
                'name' => 'Produto exemplo G',
                'description' => 'Descrição aqui',
                'price' => 11.22,
                'price_discount' => 11.00,
                'amount' => 11
            ],
            [
                'name' => 'Produto exemplo H',
                'description' => 'Descrição aqui',
                'price' => 12.22,
                'price_discount' => 12.00,
                'amount' => 36
            ],
            [
                'name' => 'Produto exemplo I',
                'description' => 'Descrição aqui',
                'price' => 65.22,
                'price_discount' => 85.00,
                'amount' => 66
            ],
            [
                'name' => 'Produto exemplo J',
                'description' => 'Descrição aqui',
                'price' => 33.22,
                'price_discount' => 33.00,
                'amount' => 33
            ]    
        ];
    }
}


$options = array(
    'uri' => 'http://localhost/webservice_soap/soap.php'
);

$server = new \SoapServer(null, $options);
$server->setObject(new Product());
$server->handle();