<?php

$listProducts = [
    [
        'name' => 'Produto exemplo A',
        'description' => 'Descrição aqui',
        'price' => 10.22,
        'price_discount' => 10.00,
        'amount' => 15
    ],
    [
        'name' => 'Produto exemplo B',
        'description' => 'Descrição aqui',
        'price' => 11.22,
        'price_discount' => 11.00,
        'amount' => 11
    ],
    [
        'name' => 'Produto exemplo C',
        'description' => 'Descrição aqui',
        'price' => 12.22,
        'price_discount' => 12.00,
        'amount' => 36
    ],
    [
        'name' => 'Produto exemplo D',
        'description' => 'Descrição aqui',
        'price' => 65.22,
        'price_discount' => 85.00,
        'amount' => 66
    ],
    [
        'name' => 'Produto exemplo E',
        'description' => 'Descrição aqui',
        'price' => 33.22,
        'price_discount' => 33.00,
        'amount' => 33
    ]    
];

echo json_encode($listProducts);