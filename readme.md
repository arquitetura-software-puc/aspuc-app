## Passo a passo para colocar em produção.



- Clone o projeto.
- Acesse o diretório do clone e adicione os módulos da aplicação que deseja instalar
- Alterado o composer, execute o comando "composer install" para adicionar as dependências.
- Execute o comando para publicar os arquivos dos módulos necessários para a aplicação.
    - Módulo usuários [php artisan vendor:publish --provider="Mgzaspuc\Users\ModuleUsers"];
    - Módulo pessoas [php artisan vendor:publish --provider="Mgzaspuc\People\ModulePeople"];
    - Módulo fornecedores [php artisan vendor:publish --provider="Mgzaspuc\Providers\ModuleProviders"];
    - Módulo produtos [php artisan vendor:publish --provider="Mgzaspuc\Products\ModuleProducts"];
- Copie o arquivo .env.example e nomeie para .env
- Configure os dados do arquivo .env conforme as informações de acesso necessária
- No terminal execute o comando php artisan key:generate;
- Para criar a estrutura de tabelas necessárias execute o comando "php artisan migrate", certifique-se da base de dados já estar criada.

## Módulo de usuários

Adicione o módulo: "mgzaspuc/users":"dev-master";

Adicione o repositório do módulo.
"repositories" : [
		{
			"type" : "git",
			"url" : "https://gitlab.com/arquitetura-software-puc/module-users.git"
		}
	]


## Módulo de pessoas
Adicione o módulo: "mgzaspuc/people": "dev-master"

Adicione o repositório do módulo.
"repositories" : [
		{
			"type" : "git",
			"url" : "https://gitlab.com/arquitetura-software-puc/module-people.git"
		}
	]


## Módulo de fornecedores
Adicione o módulo: "mgzaspuc/providers": "dev-master"

Adicione o repositório do módulo.
"repositories" : [
		{
			"type" : "git",
			"url" : "https://gitlab.com/arquitetura-software-puc/module-providers.git"
		}
	]


## Módulo de produtos
Adicione o módulo: "mgzaspuc/products": "dev-master"

Adicione o repositório do módulo.
"repositories" : [
		{
			"type" : "git",
			"url" : "https://gitlab.com/arquitetura-software-puc/module-products.git"
		}
	]