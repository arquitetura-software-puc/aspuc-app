@extends('layouts.commerce')
@section('content')
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row d-flex align-items-center flex-wrap">
            <div class="col-md-7">
                <h1 class="h2">Contato</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb d-flex justify-content-end">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Contato</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="content">
    <div class="container">
        <div class="row bar">
            <div class="col-md-12">                
                @include('elements.message_success_error')

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
                @endif
                <form method="post" action="{{ route('contato') }}">
                    @csrf
                    <div class="form-group">    
                        <label for="name">Nome:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}"/>
                    </div>
                    
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}"/>
                    </div>

                    <div class="form-group">
                        <label for="subject">Assunto:</label>
                        <input type="text" class="form-control" id="subject" name="subject" value="{{old('subject')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="message">Mensagem:</label>
                        <textarea class="form-control" id="message" name="message">{{old('message')}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Enviar mensagem</button>
                </form>
            </div>                
        </div>
    </div>
</div>
</div>
<!-- GET IT-->
@endsection
