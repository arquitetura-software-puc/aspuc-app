@extends('layouts.admin')
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row d-flex align-items-center flex-wrap">
                    <div class="col-md-7">
                        <h1 class="h2">Admin</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb d-flex justify-content-end">
                            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Admin</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            <div class="container">
                <div class="row bar">
                    <div class="col-md-3">
                        <!-- MENUS AND FILTERS-->
                        <div class="panel panel-default sidebar-menu">
                            <div class="panel-heading">
                                <h3 class="h4 panel-title">Menu</h3>
                            </div>
                            <div class="panel-body">
                                @include('elements.admin_menu')
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <p>Bem vindo  {{Auth::user() != '' ? Auth::user()->name : ''}}</p>
                    </div>

                </div>
            </div>
        </div>
        <!-- GET IT-->
@endsection
