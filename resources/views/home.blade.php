@extends('layouts.commerce')
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row d-flex align-items-center flex-wrap">
                    <div class="col-md-7">
                        <h1 class="h2">Home</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb d-flex justify-content-end">
                            <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Produtos</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            <div class="container">
                <div class="row bar">
                    <div class="col-md-3">
                        <!-- MENUS AND FILTERS-->
                        <div class="panel panel-default sidebar-menu">
                            <div class="panel-heading">
                                <h3 class="h4 panel-title">Marcas</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="nav nav-pills flex-column text-sm category-menu">
                                    @foreach($listProviders as $item)
                                    <li class="nav-item">
                                        <a href="{{url(sprintf('/home/marca/%s/%s', $item->id, str_replace(' ', '-', strtolower($item->trade_name))))}}" class="nav-link d-flex align-items-center justify-content-between">
                                            <span>{{$item->trade_name}}</span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        @if($provider)
                        <h2>{{$provider->trade_name}}</h2>
                        @endif
                        
                        <div class="row products products-big">
                            @foreach($listProducts as $product)
                            <div class="col-lg-4 col-md-6">
                                <div class="product">
                                    <a href="{{url('/produto/detalhe/' . $product->id)}}" title="Clique aqui para ver detalhes deste produto" />
                                        <div class="image">  
                                            @if($product->photo)
                                            <img src="{{url("storage/upload/img/products/{$product->photo}")}}" alt="{{$product->name}}" class="img-fluid image1" />
                                            @else
                                            <img src="{{url("img/product_default.jpg")}}" alt="{{$product->name}}" class="img-fluid image1" />                                        
                                            @endif
                                        </div>
                                        <div class="text">
                                            <h3 class="h5">
                                                {{$product->description}}                       
                                            </h3>
                                            <p class="price">R$ {{number_format($product->price, 2, ',', '.')}}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>     
                        {{ $listProducts->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- GET IT-->
@endsection
