<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <h4 class="h6">Sobre nós</h4>
                <p>Projeto desenvolvido como Trabalho de conclusã0 de curso para o curso de Arquitetura de Software Distribuídos.
                </p>
                <hr>
                <h4 class="h6">Newsletter</h4>
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-secondary"><i class="fa fa-send"></i></button>
                        </div>
                    </div>
                </form>
                <hr class="d-block d-lg-none">
            </div>
            <div class="col-lg-3">
                <h4 class="h6">Blog</h4>
                <ul class="list-unstyled footer-blog-list">
                    <li class="d-flex align-items-center">
                        <div class="image"><img src="{{url('assets/img/detailsquare.jpg')}}" alt="..."
                                                class="img-fluid"></div>
                        <div class="text">
                            <h5 class="mb-0"> <a href="post.html">Tendências 2020</a></h5>
                        </div>
                    </li>
                    <li class="d-flex align-items-center">
                        <div class="image"><img src="{{url('assets/img/detailsquare.jpg')}}" alt="..."
                                                class="img-fluid"></div>
                        <div class="text">
                            <h5 class="mb-0"> <a href="post.html">Cores primavera</a></h5>
                        </div>
                    </li>
                    <li class="d-flex align-items-center">
                        <div class="image"><img src="{{url('assets/img/detailsquare.jpg')}}" alt="..."
                                                class="img-fluid"></div>
                        <div class="text">
                            <h5 class="mb-0"> <a href="post.html">Coleção outono inverno</a></h5>
                        </div>
                    </li>
                </ul>
                <hr class="d-block d-lg-none">
            </div>
            <div class="col-lg-3">
                <h4 class="h6">Contato</h4>
                <p class="text-uppercase"><strong>ASPuc LTDA.</strong><br>Av. XV <br>Jd. América
                    <br>1000 5º Andar <br>Maringá/PR <br><strong>Brasil</strong></p><a href="contact.html"
                                                                                   class="btn btn-template-main">Ir para a página de contato</a>
                <hr class="d-block d-lg-none">
            </div>
            <div class="col-lg-3">
                <ul class="list-inline photo-stream">
                    @foreach($listProductsFooter as $product)
                    <li class="list-inline-item">
                        <a href="#">
                            @if($product->photo)
                            <img src="{{url("storage/upload/img/products/{$product->photo}")}}" alt="{{$product->name}}" class="img-fluid" />
                            @else
                            <img src="{{url("img/product_default.jpg")}}" alt="{{$product->name}}" class="img-fluid" />                                        
                            @endif                        
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="copyrights">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 text-center-md">
                    <p>&copy; {{date('Y')}}. Murillo Zampieri</p>
                </div>
                <div class="col-lg-8 text-right text-center-md">
                    <p>Template design by <a
                            href="https://bootstrapious.com/p/big-bootstrap-tutorial">Bootstrapious </a>& <a
                            href="https://fity.cz/ostrava">Fity</a></p>
                    <!-- Please do not remove the backlink to us unless you purchase the Attribution-free License at https://bootstrapious.com/donate. Thank you. -->
                </div>
            </div>
        </div>
    </div>
</footer>