<!-- Login Modal-->
<div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true"
    class="modal fade">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="login-modalLabel" class="modal-title">Acesso Restrito</h4>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                        aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                        @if (Auth::user() == '' && $errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif                                
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if (Auth::user() == '' && $errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <p class="text-center">                                
                        <button type="submit" class="login-btn">
                            <span class="d-none d-md-inline-block">Acessar</span>
                        </button>                              
                    </p>
                </form>
                <p class="text-center text-muted">Ainda não é registrado?</p>
                <p class="text-center text-muted"><a href="customer-register.html"><strong>Registre-se agora</strong></a>! 
                    É fácil e termina em 1 minuto!</p>
            </div>
        </div>
    </div>
</div>
<!-- Login modal end-->

<script>
window.onload = function() {
    @if (Auth::user() == '' && ($errors->has('email') || $errors->has('password')))
        $(window).on('load',function(){
            $('#login-modal').modal('show');
        });
    @endif 
}
</script>