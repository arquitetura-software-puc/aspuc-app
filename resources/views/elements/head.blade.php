<!-- Navbar Start-->
<header class="nav-holder make-sticky">
    <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
        <div class="container"><a href="{{url('/home')}}" class="navbar-brand home"><img
                    src="{{url('img/logomarca.png')}}" alt="Universal logo"
                    class="d-none d-md-inline-block"><img src="{{url('img/logomarca_menor.png')}}"
                    alt="Universal logo" class="d-inline-block d-md-none"><span class="sr-only">Universal - go
                    to homepage</span></a>
            <button type="button" data-toggle="collapse" data-target="#navigation"
                class="navbar-toggler btn-template-outlined"><span class="sr-only">Toggle navigation</span><i
                    class="fa fa-align-justify"></i></button>
            <div id="navigation" class="navbar-collapse collapse">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item dropdown active">
                        <a href="{{url('/')}}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="{{url('/contato')}}" class="nav-link">Contato</a>
                    </li>
                    @can('allow-admin-seller')
                    <li class="nav-item dropdown">
                        <a href="{{url('/admin')}}" class="nav-link">Dashboard</a>
                    </li>                  
                    @endcan
                </ul>
            </div>
            <div id="search" class="collapse clearfix">
                <form role="search" class="navbar-form">
                    <div class="input-group">
                        <input type="text" placeholder="Search" class="form-control"><span
                            class="input-group-btn">
                            <button type="submit" class="btn btn-template-main"><i
                                    class="fa fa-search"></i></button></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>
<!-- Navbar End-->
