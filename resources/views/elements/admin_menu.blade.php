<ul class="nav nav-pills flex-column text-sm">
  @can('allow-admin-seller')
  <li class="nav-item"><a href="{{url('/admin')}}" class="nav-link active">Dashboard</a></li>
  <li class="nav-item"><a href="{{url('/pessoas')}}" class="nav-link">Pessoas</a></li>
  @endcan()
  @can('allow-admin')
  <li class="nav-item"><a href="{{url('/fornecedores')}}" class="nav-link">Fornecedores</a></li>
  <li class="nav-item"><a href="{{url('/usuarios')}}" class="nav-link">Usuários</a></li>
  <li class="nav-item"><a href="{{url('/produtos')}}" class="nav-link">Produtos</a></li>
  @endcan()
</ul>
