<!-- Top bar-->
<div class="top-bar">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-md-6 d-md-block d-none">
                <p>{{Auth::user() != '' ? Auth::user()->name : ''}}</p>
            </div>
            <div class="col-md-6">
                <div class="d-flex justify-content-md-end justify-content-between">
                    <ul class="list-inline contact-info d-block d-md-none">
                        <li class="list-inline-item"><a href="#"><i class="fa fa-phone"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fa fa-envelope"></i></a></li>
                    </ul>
                    <div class="login">

                        @if(Auth::user() == '')
                        <a href="#" data-toggle="modal" data-target="#login-modal"class="login-btn">
                            <i class="fa fa-sign-in"></i><span class="d-none d-md-inline-block">Acessar</span>
                        </a>
                        @endif
                        @if(Auth::user() != '')
                        <a href="{{url('/logout')}}" class="btn btn-template-outlined">
                            <i class="fa fa-sign-in"></i> Sair
                        </a>
                        @endif                                       
                        <!--                                
                        <a href="customer-register.html" class="signup-btn">
                            <i class="fa fa-user"></i><span class="d-none d-md-inline-block">Cadastrar</span>
                        </a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Top bar end-->