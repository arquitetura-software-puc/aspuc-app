<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('allow-admin', function ($user) {
            return ($user->group == 'admin');
        });

        Gate::define('allow-seller', function ($user) {
            return ($user->group == 'seller');
        });

        Gate::define('allow-admin-seller', function ($user) {
            return (($user->group == 'seller') || ($user->group == 'admin'));
        });
    }
}
