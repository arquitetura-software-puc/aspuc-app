<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mgzaspuc\Products\Products;
use Mgzaspuc\Providers\Providers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($idProvider = null, $nomeProvider = null)
    {
        $products = new Products();
        $listProducts = $idProvider 
            ? $products->where(['id_provider' => $idProvider])->paginate(9)
            : $products->paginate(9);
        
        $providers = new Providers();
        $listProviders = $providers->all();  
        
        $provider = $idProvider ? $providers->find($idProvider) : null;
        
        return view('home', compact('listProducts', 'listProviders', 'provider'));
    }
}
